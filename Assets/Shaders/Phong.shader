﻿Shader "Unlit/Phong"
{
    Properties
    {
	[Header(Diffuse)]
        _MainTex ("Texture", 2D) = "white" {}
		_Diffuse ("Diffuse Color", Color) = (1,1,1,1)
		_LightColorFactor("LightColorFactor", Range(0,1)) = 0.5
	[Header(Ambient)]
		_AmbientLightCol ("Ambient Light Color", Color) = (1,1,1,1)
		_AmbienFactor("Ambient Light Strengh", Range(0,1)) = 0.5
	[Header(Specular)]
		_Shininess ("Shininess", Range(0.1, 40)) = 1
		_SpecularColor("Specular Color", Color) = (.2,.2,.2,1)
	[Header(Rim Lighting)]
		_FresnelColor ("Fresnel Color", Color) = (.2,.2,.2,1)
		_FresnelIntensity("Fresnel Intensity", Float) = 0.5
		_FPOW("Fresnel Power", Float) = 3
	[Header(Shadows)]
		_SchadowIntensity("Shadow Intensity", Range(0, 1)) = 0.6
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode" = "ForwardBase" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 pos : SV_POSITION; //muss pos heissen fuer schatten!!!
				float3 worldPos : TEXCOORD1;
				float3 worldNormal : TEXCOORD2;
				SHADOW_COORDS(5)
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _AmbientLightCol;
			float4 _Diffuse;
			float4 _LightColorFactor;
			float4 _SpecularColor;
			float _AmbienFactor;
			float _Shininess;
			float4 _FresnelColor;
			float _FresnelIntensity;
			float _FPOW;
			float _SchadowIntensity;
			


            v2f vert (appdata_base v)
            {
                v2f o;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldNormal = normalize(mul(v.normal,(float3x3)unity_WorldToObject));
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				TRANSFER_SHADOW(o)
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float3 worldNormal = i.worldNormal;

				float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
				float4 ambient = _AmbientLightCol * _AmbienFactor;

				float4 NdotL = saturate(dot(worldNormal, lightDir));

				float4 diffuse = _Diffuse * NdotL * _LightColor0;
				
				diffuse = lerp(diffuse, _LightColor0 * diffuse, _LightColorFactor);

				
				float3 refl = normalize(reflect(-lightDir, worldNormal));
				float  rDotV = 0.5 *dot(refl, viewDir) + 0.5 ;

				float3 HalfVector = normalize(lightDir + viewDir);
				float NDotH = max(0,dot(worldNormal, HalfVector));
				_Shininess *= _Shininess;

				float4 specular = pow(NDotH, _Shininess) * _SpecularColor * _LightColor0;

				float schadow = SHADOW_ATTENUATION(i);
				float4 light = (diffuse + ambient) * lerp(1, schadow, _SchadowIntensity);
				float fresnel = _FresnelIntensity * (1- (0.5 * dot(worldNormal, viewDir) + 0.5));
				fresnel = pow(fresnel, _FPOW);
				light = lerp(light, _FresnelColor , fresnel);

                fixed4 col = tex2D(_MainTex, i.uv) * light  + specular;
                return col;
			}
            ENDCG
        }
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}
